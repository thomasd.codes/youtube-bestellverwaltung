<?php

namespace App\Repository;

use App\Entity\OrderLineItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrderLineItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderLineItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderLineItem[]    findAll()
 * @method OrderLineItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @extends ServiceEntityRepository<OrderLineItem>
 */
class OrderLineItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderLineItem::class);
    }
}

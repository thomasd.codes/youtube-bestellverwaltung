<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\OrderRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 */
class Order
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @phpstan-ignore-next-line
     */
    private int $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTimeInterface $created;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $status;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $sendImage;

    /**
     * @ORM\OneToMany(targetEntity=OrderLineItem::class, mappedBy="orderEntity", orphanRemoval=true)
     * @var Collection<int, OrderLineItem> $orderLineItems
     */
    private Collection $orderLineItems;

    /**
     * @ORM\OneToOne(targetEntity=Recipient::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private Recipient $recipient;

    /**
     * @ORM\ManyToOne(targetEntity=Company::class, inversedBy="orders")
     * @ORM\JoinColumn(nullable=false)
     */
    private Company $company;

    public function __construct()
    {
        $this->orderLineItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreated(): ?DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(DateTimeInterface $created): self
    {
        $this->created = $created;
        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getSendImage(): ?string
    {
        return $this->sendImage;
    }

    public function setSendImage(?string $sendImage): self
    {
        $this->sendImage = $sendImage;

        return $this;
    }

    /**
     * @return Collection<int, OrderLineItem>
     */
    public function getOrderLineItems(): Collection
    {
        return $this->orderLineItems;
    }

    public function addOrderLineItem(OrderLineItem $orderLineItem): self
    {
        if (!$this->orderLineItems->contains($orderLineItem)) {
            $this->orderLineItems[] = $orderLineItem;
            $orderLineItem->setOrderEntity($this);
        }

        return $this;
    }

    public function removeOrderLineItem(OrderLineItem $orderLineItem): self
    {
        $this->orderLineItems->removeElement($orderLineItem);
        return $this;
    }

    public function getRecipient(): ?Recipient
    {
        return $this->recipient;
    }

    public function setRecipient(Recipient $recipient): self
    {
        $this->recipient = $recipient;

        return $this;
    }

    public function getCompany(): Company
    {
        return $this->company;
    }

    public function setCompany(Company $company): self
    {
        $this->company = $company;

        return $this;
    }
}
